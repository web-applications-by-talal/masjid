import axios from 'axios';
import router from './router/router'


const instance = axios.create({
  baseURL: "http://"+process.env.VUE_APP_DOMAIN,

});

instance.interceptors.response.use((response) => {
  return response;
}, (error) => {
  if(error.response.status === 401) {
    alert("You are not authorized! please login and try again");
    localStorage.removeItem("token")
    router.push("/login")
  }
  else if (error.response.status != 200 || error.response == null) {
      alert("Error - Values have NOT been saved corectly!");
      console.error(error.response.data);
  }
  return Promise.reject(error);
});

export default instance;