import Vue from 'vue'
import Element from 'element-ui'
import "element-ui/lib/theme-chalk/index.css"
import locale from 'element-ui/lib/locale/lang/en';
import App from './components/App.vue'
import router from './router/router'

locale.el.pagination.pagesize = ''

Vue.use(Element, {locale});

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
