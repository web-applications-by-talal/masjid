import Vue from 'vue'
import Router from 'vue-router'
import FrontPage from '../components/FrontPage.vue'
import EditPage from '../components/edit/EditPage.vue';
import Login from '../components/LoginPage.vue';

Vue.use(Router);

export default new Router({
  mode: 'history',
  hash: false,
  routes: [
    {
        path: '/',
        component: FrontPage
    },
    {
        path: '/edit',
        component: EditPage
    },
    {
        path: '/login',
        component: Login,
    },
    { 
        path: '*', 
        redirect: '/' 
    }, // catch all use case
  ]
});