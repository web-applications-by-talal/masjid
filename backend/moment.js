const moment = require('moment')
const now = moment()

const date = now.format("YYYY/MM/DD")
const day = now.format("D")
const month = now.format("M")
const year = now.format("Y")

console.log("date is", date)
console.log("day is", day)
console.log("month is", month)
console.log("year is",year)
