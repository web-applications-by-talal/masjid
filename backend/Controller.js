const express = require('express')
const app = express()
// const path = require('path')
const logger = require('fancy-log')
const bodyParser = require('body-parser')
const cors = require('cors')

app.use(cors())
// ============ Allow Requests from a Browser ==========
app.use(bodyParser.json()); // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded
app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header('Access-Control-Allow-Methods', 'PUT, POST, GET, DELETE, OPTIONS');
    res.header("Access-Control-Allow-Headers", "Origin, Access-Control-Allow-Headers, Authorization, X-Requested-With, Content-Type, Accept, id, jwt");
    next()
})

// Logger Middleware    
app.all('*', (req, res, next) => {
    logger(`${req.method} - [${req.url}]`)
    next();
})

app.get('/', (req, res) => {
    res.status(200)
    res.json({ message: 'healthy' })
})


const auth = require('./controllers/auth')
app.post('/login', auth.authenticateUser)

const prayerController = require('./controllers/prayer')
app.get('/prayertimes', prayerController.getAll)

const fileServer = require('./controllers/fileServer')
app.post('/images', auth.authorizeUser, fileServer.saveImage) //upload images
app.get('/images', auth.authorizeUser, fileServer.getImageNames) // get a list of file names

const ImageDirectory = fileServer.uploadFolder
console.log(`ImageDirectory => ${ImageDirectory}`)
app.use(express.static(ImageDirectory)) // middleware to get a actual image by name

const commController = require('./controllers/communication')
app.post('/iqama', auth.authorizeUser, commController.setIqama)
app.get('/iqama', commController.getIqama)
app.delete('/iqama/:prayerName', auth.authorizeUser, commController.deleteIqama)


let port = process.env.PORT

server = app.listen(port, () => {
    logger('backend started on port ' + port)
})
module.exports = server;