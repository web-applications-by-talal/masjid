const multer = require('multer')
const fs = require('fs')

module.exports.uploadFolder = "./image-uploads"

// SET STORAGE
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, uploadFolder)
    },
    filename: function (req, file, cb) {
        // let startingPoint = file.originalname.lastIndexOf('.')
        // let fileType = file.originalname.slice(startingPoint)
        // const filename = file.originalname + '-' + Date.now() + fileType
        cb(null, file.originalname)
    }
})

const imageUploader = multer({
    storage,
    fileFilter: (req, file, callback) => {
        const t = file.mimetype
        if (t == 'image/jpeg' || t == 'image/jpg' || t == 'image/png') {
            callback(null, true)
        } else {
            const errMsg =
                'wrong myme type, you are trying to upload ' +
                'a file format other than jpeg or png'

            callback(new Error(errMsg), false)
        }
    }
})

// const upload = multer({ storage: storage })

const singleUpload = imageUploader.single('myImage')


module.exports.saveImage = (req, res) => {

    return singleUpload(req, res, (err) => {
        if (err instanceof multer.MulterError) {
            console.log("multer error while uploading")
        } else if (err) {
            console.log("non-multer error while uploading")
        } else {
            console.log("image uploaded succesfully =>", req.file.filename)
            return res.status(200)
                .json({ success: true, message: "image uploaded" })
        }

        // if you reach here, there was an err thrown
        console.log("error =>", err)

        let message
        if (err.message = "wrong myme type, " +
            "you are trying to upload a file format other than jpeg or png") {
            message = err.message
        } else {
            message = err
        }

        return res.status(400)
            .json({ success: false, message })
    })
}

module.exports.getImageNames = (req, res) => {
    let fileNames
    try {
        fileNames = fs.readdirSync(this.uploadFolder)

    } catch (err) {
        console.log(`err => `, err)
        return res
            .status(500)
            .json({
                success: false,
                message: err.message,
            })
            .end()
    }
    console.log("fileNames =>", fileNames)
    return res
        .status(200)
        .json({
            success: true,
            message: "image filenames retrieved",
            data: fileNames
        })
        .end()
}
