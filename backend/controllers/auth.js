const jwt = require("jsonwebtoken")
const fs = require('fs')

let superSecret = process.env.JWTSECRET
let USERNAME = process.env.USERNAME
let PASSWORD = process.env.PASSWORD

if (process.env.DEPLOYMENT == 'prod') {
    superSecret = fs.readFileSync(process.env.JWTSECRET)
    USERNAME = fs.readFileSync(process.env.USERNAME)
    PASSWORD = fs.readFileSync(process.env.PASSWORD)
}
module.exports.authenticateUser = (req, res) => {
    console.log("headers =>", req.headers)
    const { username, password } = req.body
    if (username == USERNAME && password == PASSWORD) {
        const token = jwt.sign(
            { username },
            superSecret,
            { expiresIn: 60 * 60 }
        )
        return res.json({ success: true, token })

    }
    return res.json({ success: false, message: "wrong credentials" })
}

module.exports.verifyToken = (token) => {
    let decoded = null
    let message = null
    if (token == null || token == undefined) {
        message = "no jwt sent"
        console.log(message)
        return { isAuthorized: false, message }
    }
    try {
        decoded = jwt.verify(token, superSecret)
    } catch (err) {
        message = err.message
        return { isAuthorized: false, message }
    }
    return { isAuthorized: true, token: decoded }
}

module.exports.authorizeUser = (req, res, next) => {
    console.log("trying to authenticate user...")
    console.log(req.headers["jwt"])

    try {
        const token = req.headers['jwt']

        const result = this.verifyToken(token)
        console.log(result)
        if (result.isAuthorized) {
            next()
        } else {
            throw new Error("token cannot be verified")
        }
    } catch (err) {
        console.log(err.message)
        return res
            .status(401)
            .json({ success: false, message: "not authorized: " + err.message })
            .end()
    }

}