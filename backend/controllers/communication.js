const fs = require('fs')
const prayerController = require('./prayer')
const moment = require('moment')

// useful constants
const fajr = "Fajr"
const sunrise = "Sunrise"
const dhuhr = "Dhuhr"
const asr = "Asr"
const maghrib = "Maghrib"
const isha = "Isha"
// articled used to write json files 
// https://stackabuse.com/reading-and-writing-json-files-with-node-js/

// folder in which data will be stored
const dataFolder = "./communication.data/"

// A map with key => value pairs like the following:
// fajr => time, where
// key is a string
// time is { hour: 0-24, min: 0-59}
const iqamaFile = dataFolder + 'iqama.json'

// An array of messages which need to be displayed on slideshow
const messageFile = dataFolder + 'message.json'

// A map with key => value pairs like the following:
// <class name> => time, where
// key is a string
// time is {day: monday-saturday, hour: 0-24, min: 0-59}
const darsFile = dataFolder + 'dars.json'

// Ensures that the passed string is a folder name which exists. 
// If 'folder' doesn't exist, it creates it
const ensureDataFolder = (folder) => {
    if (fs.existsSync(folder)) { return }

    fs.mkdirSync(folder)
}

const ensureFileExist = (filename) => {
    if (fs.existsSync(filename)) { return }

    fs.writeFileSync(filename, "{}")
}

ensureDataFolder(dataFolder)
ensureFileExist(iqamaFile)
ensureFileExist(messageFile)
ensureFileExist(darsFile)


module.exports.setIqama = (req, res) => {
    let result = {
        success: false,
        message: "default empty message",
        data: {}
    }
    let iqamaTimes = req.body
    const validated = validateTime(iqamaTimes)
    if (!validated.success) {
        console.log("iqamaTimes input are invalid")
        result.success = validated.success
        result.message = validated.message
        result.data = validated.data
        return res.json(result).end()
    }
    let mergedTimes = {}
    try {
        let existingTimes = JSON.parse(fs.readFileSync(iqamaFile))
        console.log("iqamaTimes =>", iqamaTimes)
        console.log("existingTimes =>", existingTimes)
        mergedTimes = merge(existingTimes, iqamaTimes)

        try {
            fs.writeFileSync(iqamaFile, JSON.stringify(mergedTimes))
            const log = "successfully saved the new Iqama times"
            console.log(log)
            result.success = true
            result.message = log
            result.data.merged = mergedTimes

        } catch (err) {
            console.log("error =>", err.message)
            result.success = false
            result.message = err.message
            result.data.mergedTimes = mergedTimes
        }
    } catch (err) { // means that iqamaFile DNE
        console.log("error =>", err.message)

        try {
            console.log("stringify =>", JSON.stringify(iqamaTimes))
            fs.writeFileSync(iqamaFile, JSON.stringify(iqamaTimes))
            result.success = true
            result.message = "iqama times successfully saved"
            result.data.iqamaTimes = iqamaTimes

        } catch (err) {
            console.log("error writing iqama times to local file =>", err.message)
            result.success = false
            result.message = err.message
            result.data = null
        }
    }
    // console.log("at the end of request")
    // console.log("iqamaTimes =>", iqamaTimes)
    // console.log("result =>", result)

    return res.json(result).end()
}


module.exports.getIqama = async (req, res) => {
    let existingRecord = {}
    const prayerApiResult = await prayerController.getPrayerTimes()
    const prayerTimes = prayerApiResult.data.timings
    const cleanTimes = clean(prayerTimes)

    let result = {
        success: false,
        message: "",
        data: {
            timings: {}
        }
    }
    try {
        existingRecord = JSON.parse(fs.readFileSync(iqamaFile))
    } catch (err) {
        console.log("an error occured while trying to open iqama file=>")
        console.log(err.message)
        // if the file doesn't exist, iqama times have not been set.
        // therefore just use the standard iqama time rules
        existingRecord = {}
    }

    resultIqama = generateIqamaTime(cleanTimes, existingRecord)
    result.success = true
    result.message = "operation successful"
    result.data.timings = resultIqama

    return res.json(result).end()
}

// Send the name of the iqama to be deleted as a paramater in the url like so:
// delete /iqama/Fajr or /iqama/Dhuhr for example.
module.exports.deleteIqama = async (req, res) => {
    iqamaToBeDeleted = req.params.prayerName
    console.log('iqamaToBeDeleted =>', iqamaToBeDeleted)

    iqamaNames = ['Fajr', 'Dhuhr', 'Asr', 'Maghrib', 'Isha']
    if (iqamaToBeDeleted.toLowerCase() == 'reset') {
        try {
            fs.writeFileSync(iqamaFile, JSON.stringify('{}'))
        } catch (err) {
            console.log("Caught error: =>", err)
            return res.status(500).json({ success: false, message: err })
        }
        return res.status(200).json({ success: true, message: 'successfully reset all iqamas' })
    }
    else if (!iqamaNames.includes(iqamaToBeDeleted)) {
        return res.json({
            success: false,
            message: `${iqamaToBeDeleted} is not a valid prayer time. Should be one of 'Fajr', 'Dhuhr', 'Asr', 'Maghrib', 'Isha'`,
        })
    }

    let existingTimes



    try {
        existingTimes = await JSON.parse(fs.readFileSync(iqamaFile))
    } catch (err) {
        console.log(err)
        console.log("nothing to delete")
        return res.json({ success: true, message: 'nothing to delete' })
    }
    delete existingTimes[iqamaToBeDeleted]

    try {
        fs.writeFileSync(iqamaFile, JSON.stringify(existingTimes))
    } catch (err) {
        console.log(err)
        return res.json({ success: false, message: "Error writing to file after deletion: '" + err + "'" })
    }

    return res.json({ success: true, message: iqamaToBeDeleted + ' iqama time deleted' })

}




// existing and incoming are meant to be two iqama times objects
const merge = (existing, incoming) => {
    let merged = {}
    for (let key in existing) {
        merged[key] = existing[key]
    }
    for (let key in incoming) {
        merged[key] = incoming[key]
    }
    return merged

}

// makes sure that the format is correct for setting iqama times
const validateTime = (timings) => {
    let result = {}
    result.success = true
    result.message = "iqama times format validated"
    result.data = timings
    let correctMatch
    let x
    const regex = /^([01]\d|2[0-3]):[0-5]\d$/

    for (let key in timings) {

        // console.log("key =>", key)
        if (key != 'Fajr' &&
            key != 'Dhuhr' &&
            key != 'Asr' &&
            key != 'Maghrib' &&
            key != 'Isha') {
            result.success = false
            result.message = "Attribute '" + key + "' is not an accepted key for iqama times. "
                + "Please provide one of the following: "
                + "'Fajr', 'Dhuhr', 'Asr', 'Maghrib', 'Isha'."
            result.data = {}
            break
        }

        // ensure correct format
        correctMatch = timings[key].search(regex) === 0 //because the regex should match at 0th index
        // console.log("timings =>", timings)

        if (!correctMatch) {
            result.success = false
            result.message = `incorrect iqama time '${timings[key]}', input time should be in 24h format`
            result.data = undefined
            break
        }

    }
    const timingsIsEmpty = JSON.stringify(timings) === JSON.stringify({})
    if (timingsIsEmpty) {
        result.success = false
        result.message = "timings has none of the correct key values: " +
            "'Fajr', 'Dhuhr', 'Asr', 'Maghrib', 'Isha'."
        result.data = {}
    }

    return result

}


const generateIqamaTime = (prayerTimes, existingTimes) => {
    // helper stuff 
    // ------------------------------
    const map = {
        "Fajr": 0,
        "Sunrise": 1,
        "Dhuhr": 2,
        "Asr": 3,
        "Maghrib": 4,
        "Isha": 5
    }
    const getKeyByValue = (object, value) => {
        return Object.keys(object).find(key => object[key] === value);
    }
    // ------------------------------

    let resultTimes = {}
    for (let key in prayerTimes) {
        if (key == sunrise) {
            continue //because there is no iqama time for sunrise
        }
        // console.log("key =>", key)

        let lowerBoundTime = moment()
            .hours(prayerTimes[key].slice(0, 2))
            .minutes(prayerTimes[key].slice(3, prayerTimes[key].length))

        // console.log("lowerBoundTime =>", lowerBoundTime)
        // console.log(`existingTimes[${key}] =>`, existingTimes[key])

        let defaultIqamaRulesNeeded = false

        if (existingTimes[key] != undefined) { // there exists a saved time for that key
            let savedTime = existingTimes[key]
            let nextPrayer = getKeyByValue(map, (map[key] + 1) % 6) // the next prayer time, key={0..5}

            let onDiskTime = moment()
                .hours(savedTime.slice(0, 2))
                .minutes(savedTime.slice(3, savedTime.length))
            let upperBoundTime = moment()
                .hours(prayerTimes[nextPrayer].slice(0, 2))
                .minute(prayerTimes[nextPrayer].slice(3, prayerTimes[nextPrayer].length))

            if (key == 'Isha') {
                upperBoundTime = moment().hours('23').minute('59')
                // because the next prayer is fajr and the upperbound time will
                // always be lower than Isha time so the timeWithinBound Flag
                // will always be true. Therefore, it will be impossible to set
                // an custom isha iqama time.
            }
            const timeWithinBound = onDiskTime.isAfter(lowerBoundTime) && onDiskTime.isBefore(upperBoundTime)
            // console.log("nextPrayer =>", nextPrayer)
            // console.log("upperBoundTime =>", upperBoundTime)
            // console.log("timeWithinBound =>", timeWithinBound)
            if (timeWithinBound) {
                resultTimes[key] = existingTimes[key]
                // because it means that there exists an iqama time still valid
            } else {
                defaultIqamaRulesNeeded = true
            }
        } else { // iqama time on disk is outside of boundaries
            defaultIqamaRulesNeeded = true
        }
        if (defaultIqamaRulesNeeded) {
            let offset = 0
            switch (key) {
                case fajr: offset = 20
                    break
                case maghrib: offset = 5
                    break
                case dhuhr:
                case asr:
                case isha:
                    offset = 10
                    break
                default:
                    throw new Error("There is no such key possible => " + key)
            }
            lowerBoundTime.add(offset, 'minutes')
            // console.log("+ offset =>", lowerBoundTime)
            resultTimes[key] = lowerBoundTime.format("HH:mm")
        }
        // console.log(`resulTimes[${key}] =>`, resultTimes[key])
        // console.log("-----------------------------------\n")
    }

    return resultTimes
}



const clean = (times) => {
    // transforms first object into second
    // { 
    //      Fajr: '04:55 (EDT)',
    //      Sunrise: '06:21 (EDT)',
    //      Dhuhr: '12:53 (EDT)',
    //      Asr: '16:33 (EDT)',
    //      Sunset: '19:25 (EDT)',
    //      Maghrib: '19:25 (EDT)',
    //      Isha: '20:51 (EDT)',
    //      Imsak: '04:45 (EDT)',
    //      Midnight: '00:53 (EDT)' 
    // }
    // { 
    //      Fajr: '04:55',
    //      Sunrise: '06:21',
    //      Dhuhr: '12:53',
    //      Asr: '16:33',
    //      Maghrib: '19:25',
    //      Isha: '20:51'
    // }
    let cleaned = {}
    for (let key in times) {
        if (key != 'Fajr' &&
            key != 'Sunrise' &&
            key != 'Dhuhr' &&
            key != 'Asr' &&
            key != 'Maghrib' &&
            key != 'Isha') {
            // because prayer api returns keys such as 'sunrise' which are useless
            continue
        }
        cleaned[key] = times[key].slice(0, 5)
    }
    return cleaned
}