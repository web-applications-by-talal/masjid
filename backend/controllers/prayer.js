const axios = require('axios')
const moment = require('moment')
const endpoint = "http://api.aladhan.com/v1/calendarByCity"

// this is the article used to make implement promises
// https://usefulangle.com/post/170/nodejs-synchronous-http-request
module.exports.getAll = async(req, res) => {

    const prayerTimes = await this.getPrayerTimes()
    res.json(prayerTimes)
    res.end()

}

module.exports.getPrayerTimes = async () => {

    try {
        let promise = prayerTimesPromise()
        let prayerTimesResponse = await promise
        console.log('prayerTimesResponse =>', prayerTimesResponse)
        return prayerTimesResponse
    } catch (err) {
        console.log("Prayer times promise was rejected:")
        console.log(err)
    }

}

function prayerTimesPromise() {
    return new Promise((resolve, reject) => {
        let timings

        //using moment.js
        const now = getNow()

        axios.get(endpoint, {
            params: {
                city: "Laval",
                country: "Canada",
                state: "Quebec",
                month: now.month,
                year: now.year,
                method: 2,
            }
        })
            .then((response) => {
                // console.log("response object is =>")
                // console.log(response)
                timings = response.data.data[now.day - 1]
                // console.log(`axios response.data.data[${now.day - 1}] is : =>`, timings)
                resolve({ data: timings })
            })

            .catch((error) => {
                console.log("axios error is : =>", error)
                reject({ myError: error })
            })
    })
}

const getNow = () => {
    const now = moment()
    const date = now.format("YYYY/MM/DD")
    const day = now.format("D")
    const month = now.format("M")
    const year = now.format("Y")

    return { date, day, month, year }
}