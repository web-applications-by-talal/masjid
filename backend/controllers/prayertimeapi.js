const request = require('superagent');


class api {

    constructor(endpoint){
        this.endpoint = endpoint
    }

    async getPrayerTimes(param){
        return request
        .get(this.endpoint)
        .send(param)
        .set('accept', 'json')
        .end()
    }
}
module.exports.api