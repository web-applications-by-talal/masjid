.PHONY: build-backend build-frontend run-swarm rm-swarm all

build-backend:
	docker build -f backend.dockerfile . -t masjid-backend

build-frontend:
	docker build -f frontend.dockerfile . -t masjid-frontend

run-swarm: rm-swarm
	sleep 10
	docker stack deploy --compose-file itissam.swarm.yml --with-registry-auth masjid

rm-swarm:
	docker stack rm masjid

all: build-backend build-frontend run-swarm

run-traefik:
	docker stack deploy --compose-file traefik.swarm.yml --with-registry-auth proxy
