# MosqueWebApp

This project was generated with Node & VueJS.

## Run Development server Locally

Go to backend folder and run `npm i`.

Then Run `npm run dev` for a dev server. Navigate to `http://localhost:8090/`. The app will automatically reload if you change any of the source files.

## Run Frontend Locally

Change the Domain Name for dev environment by going to the `frontend-vue` folder.

`vim .env` and set the `VUE_APP_DOMAIN=localhost:8090` to connect to backend port

Also, set `USERNAME=<desired_login_credentials>` and `PASSWORD=<desired_login_credentials>` to login.

Run `npm i` inside the frontend-vue folder.

Then Run `npm run serve` inside the frontend-vue folder and navigate to `http://localhost:8080/`.

