FROM node:12.2.0-alpine

RUN mkdir /app/

COPY ./backend/ /app/backend

RUN cd /app/backend \
    && npm install 

# if process.env.DEPLOYMENT=prod node will start on port 80
EXPOSE 80 
WORKDIR /app/backend
CMD ["npm","start"]