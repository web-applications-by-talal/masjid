# base image
FROM node:12.2.0

# set working directory
WORKDIR /frontend-vue

# add `/app/node_modules/.bin` to $PATH
ENV PATH /app/frontend-vue/node_modules/.bin:$PATH

# install and cache app dependencies
COPY frontend-vue/ .

RUN npm install \
    && npm install @vue/cli@3.7.0 -g \
    && npm audit fix

# start app
CMD ["npm", "run", "serve"]
